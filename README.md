# POC CronosWatcher

This bot watches the Cronos Chain and flags out new EbisusBay listings on watched collections

## Involved RPCs

We use the VVS RPC available at `https://rpc.vvs.finance`.

## Involved contracts

| Name               | Type           | Contract Address                             |
| ------------------ | -------------- | -------------------------------------------- |
| Ebisus Bay         | Marketplace    | `0x7a3CdB2364f92369a602CAE81167d0679087e6a3` |
| Degen Mad Meerkat  | NFT colletion  | `0xa19bfce9baf34b92923b71d487db9d0d051a88f8` |
| Mad Meerkat Burrow | NFT collection | `0x89dbc8bd9a6037cbd6ec66c4bf4189c9747b1c56` |
| MM Treehouse       | NFT collection | `0xDC5bBDb4A4b051BDB85B959eB3cBD1c8C0d0c105` |

## Resources

https://goethereumbook.org/en/
