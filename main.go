package main

import (
	"context"
	"encoding/hex"
	"fmt"
	"log"
	"math/big"
	"strings"
	"time"

	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"

	_ "embed"
)

const (
	abiOfInterest = `
[
  {
    "inputs": [
      {
        "internalType": "address",
        "name": "tokenAddress",
        "type": "address"
      },
      {
        "internalType": "uint256",
        "name": "tokenId",
        "type": "uint256"
      },
      {
        "internalType": "uint256",
        "name": "amount",
        "type": "uint256"
      }
    ],
    "name": "NewListing",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "amount",
        "type": "uint256"
      }
    ],
    "name": "Sale",
    "type": "function"
  }
]
`
	ContractAddressEbisusBay  = "0x7a3CdB2364f92369a602CAE81167d0679087e6a3"
	MethodSignatureEbisusList = "79c7550f"
	MethodSignatureEbisusSale = "c4175a44"

	targetBlock = 3003657
)

var whitelist = map[string]string{
	"0xDC5bBDb4A4b051BDB85B959eB3cBD1c8C0d0c105": "MM Treehouse",
	"0xa19bfce9baf34b92923b71d487db9d0d051a88f8": "Mad Meerkat Degen",
	"0x89dBC8Bd9a6037Cbd6EC66C4bF4189c9747B1C56": "Mad Meerkat",
}

type NewListingEvent struct {
	TokenAddress common.Address
	TokenID      *big.Int
	Amount       *big.Int
}

type SaleEvent struct {
	Amount *big.Int
}

func main() {
	client, err := ethclient.Dial("https://rpc.vvs.finance")
	if err != nil {
		log.Fatal(err)
	}
	listingAbiParsed, err := abi.JSON(strings.NewReader(abiOfInterest))
	if err != nil {
		log.Fatal(err)
	}

	currentBlock := big.NewInt(0)
	log.Printf("sniping...")
	for {
		header, err := client.HeaderByNumber(context.Background(), nil)
		if err != nil {
			log.Printf("[!!!] failed to get latest header: %s", err)
			continue
		}
		if big.NewInt(0).Sub(header.Number, currentBlock).Int64() > 0 {
			block, err := client.BlockByNumber(context.Background(), currentBlock)
			if err != nil {
				log.Printf("[!!!] failed to get latest header: %s", err)
				continue
			}
			transactions := block.Transactions()
			for _, transaction := range transactions {
				if transaction.To().Hex() == ContractAddressEbisusBay {
					transactionData := fmt.Sprintf("%x", transaction.Data())
					isListing := strings.Index(transactionData, MethodSignatureEbisusList) == 0
					if isListing {
						decodedData, err := hex.DecodeString(transactionData[8:])
						if err != nil {
							log.Printf("failed here")
							log.Fatal(err)
						}
						parsedData, err := listingAbiParsed.Methods["NewListing"].Inputs.Unpack(decodedData)
						if err != nil {
							log.Fatal(err)
						}
						li := NewListingEvent{
							TokenAddress: parsedData[0].(common.Address),
							TokenID:      parsedData[1].(*big.Int),
							Amount:       parsedData[2].(*big.Int),
						}
						collectionAddress := li.TokenAddress.Hex()
						label := collectionAddress
						if commonName, available := whitelist[label]; available {
							label = commonName
						}
						tokenId := li.TokenID.Int64()
						price := big.NewInt(0).Div(li.Amount, big.NewInt(1000000000000000000)).Int64()
						log.Printf("[%s #%v listed at %v cro](https://app.ebisusbay.com/collection/%s/%v)", label, tokenId, price, collectionAddress, tokenId)
					}
				}
			}
			currentBlock = header.Number
		}
		<-time.After(1 * time.Second)
	}
}
